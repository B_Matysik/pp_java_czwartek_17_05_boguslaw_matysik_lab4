package trojkat;

import javax.swing.*;
import java.awt.*;
import java.util.Random;

/**
 * Created by Boguslav on 05.04.2017.
 */
public class Trojkat extends Canvas {
    int x=512, y=382, x1=512, y1=109, x2=146, y2=654, x3=876, y3=654;
    int dx, dy;
    Random random= new Random();
    int nPowtorzen=50000;
    double odstep = (double)(x3-x2)/nPowtorzen;
    int punktX;
    String txt ="";

    public void paint(Graphics g){
        g.setFont(new Font("Calibri Light", Font.PLAIN, 15));

        for(int i=0; i<nPowtorzen+1;i++) {
            try {
                Thread.sleep(0,1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            int liczba = (random.nextInt(3)+1);
           g.drawLine(x,y,x,y);
           if(liczba==1){
               dx = x-x1;
               dy = y-y1;
           }else if(liczba==2){
                dx = x-x2;
                dy = y-y2;
            }else if(liczba==3){
               dx = x-x3;
               dy = y-y3;
           }
           x=x-dx/2;
            y=y-dy/2;
            //pasek postepu
            punktX = (int)(x2+odstep*i);
            g.drawLine(punktX,685, punktX, 700);
            if (i%500==0){
                txt = "Trwa rysowanie Trójkata Sierpienskiego zbudowanego z "+ i + "/"+nPowtorzen+" punktow";
                g.clearRect(146,658,800,25);
                g.drawString(txt,146,675);

        }
        }

        g.setFont(new Font("Calibri Light", Font.PLAIN, 30));
        g.drawString("Trójkata Sierpienskiego został narysowany !", 260, 60);
        //Image image = new ImageIcon("C:\\Users\\Boguslav\\Pictures\\image.jpg").getImage();
        //g.drawImage(image, 100,100, 300, 300, this);



    }

    public static void main(String[] args) {
        JFrame jf = new JFrame("Trojkat Sierpienskiego");
        Trojkat t = new Trojkat();
        jf.setSize(1024,768);
        jf.setVisible(true);
        jf.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        jf.add(t);


    }
}
