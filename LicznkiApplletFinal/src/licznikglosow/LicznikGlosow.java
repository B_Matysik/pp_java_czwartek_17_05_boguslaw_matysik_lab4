package licznikglosow;
// ***********************************************************************
//  LicznikGlosow.java
//
//  Wykorzystuje GUI oraz listenery eventow do glosowania
//  na dwoch kandydatow -- Jacka i Placka.
//
// *************************
// **********************************************

import java.awt.*;
import java.awt.event.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;

public class LicznikGlosow extends JApplet
{
    private static Socket s;
    Scanner input;
    PrintWriter output;
    private int APPLET_WIDTH = 300, APPLET_HEIGHT=400;
    private JLabel labelJacek;
    private JButton Jacek;
    private JLabel labelPlacek;
    private JButton Placek;

    // ------------------------------------------------------------
    //  Ustawia GUI
    // ------------------------------------------------------------
    public void init ()
    {
        try {
            s = new Socket("localhost", 5555);
        } catch (IOException ex) {
            Logger.getLogger(LicznikGlosow.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {
            output = new PrintWriter(s.getOutputStream(),true);
        } catch (IOException ex) {
            Logger.getLogger(LicznikGlosow.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {
            input = new Scanner(s.getInputStream());
        } catch (IOException ex) {
            Logger.getLogger(LicznikGlosow.class.getName()).log(Level.SEVERE, null, ex);
        }

        Jacek = new JButton ("Glosuj na Jacka!");
        Jacek.addActionListener (new JacekButtonListener());
        labelJacek = new JLabel ("Glosy dla Jacka: 0");

        Placek = new JButton ("Glosuj na Placka!");
        Placek.addActionListener (new PlacekButtonListener());
        labelPlacek = new JLabel ("Glosy dla Placka: 0");

        Container cp = getContentPane();
        cp.setLayout (new FlowLayout());
        cp.add (Jacek);
        cp.add (labelJacek);
        cp.add (Placek);
        cp.add (labelPlacek);

        setSize (APPLET_WIDTH, APPLET_HEIGHT);
    }


    // *******************************************************************
    //  Reprezentuje listener dla akcji wcisniecia przycisku
    // *******************************************************************
    private class JacekButtonListener implements ActionListener
    {
        public void actionPerformed (ActionEvent event)
        {
            output.println(1);
            labelJacek.setText ("Glosy dla Jacka: " + input.nextInt());
            repaint ();
        }
    }

    private class PlacekButtonListener implements ActionListener
    {
        public void actionPerformed (ActionEvent event)
        {
            output.println(0);
            labelPlacek.setText ("Glosy dla Placka: " + input.nextInt());
            repaint ();
        }
    }
}
