//package BlackJack;
/**
 * Created by bogus on 23.03.2017.
 */
// wszystkie wartosci do funki przekazywane jako nr wylosowanej karty
public class Karta {
    int nrKarty;            //wylosowany nr karty
    int nrKartyWTali;       //nr karty w tali
    int wartosc;            //wartosc karty od 0 do 12
    int wartoscWBlackJack;  //wartosc karty w BlackJack od 2 do 10 bez uwzględnienia 2 opcji dla asa ( dla asa przyjęto 11)
    String kolor;           //kolor karty
    String postac;          //postac karty od 2 do Ace
    boolean visible=false;  //czy karta jest widoczna na stole

    public Karta(){}
    //konstruktor na podstawie wylosowanej liczby 52*liczbaTali
    public Karta (int a, boolean widoczna){
        this.visible=widoczna;
        this.nrKarty=a;
        this.nrKartyWTali=a%52;
        this.wartosc=(a%52)%13;
        wyliczKolor(a);     // wylicza i ustawia kolor
        wyliczPostac(a);    // wylicza i ustawia nazwe postaci
        wyliczWartoscWBlackJack(a); // wylicza i ustawia warotsc WBlackJack (as 11)
    }
    //wylicza kolor karty
    void wyliczKolor(int a){
        a=a%52;
        if(a>=0 && a<=12)
            this.kolor="clubs";
        else if (a>=13 && a<=25)
            this.kolor="diamonds";
        else if (a>=26 && a<=38)
            this.kolor="hearts";
        else if (a>=39 && a<=51)
            this.kolor="spades";
        else this.kolor="karta zostala zle zinicjowana";
    }
    // wylicza i ustawia nazwe postaci
    void wyliczPostac(int b){
        int a=(b%52)%13;
        if (a>=2 && a<=10)
            this.postac = Integer.toString(this.wartosc);
        else if(a==11)
            this.postac = "jack";
        else if(a==12)
            this.postac = "king";
        else if(a==0)
            this.postac = "queen";
        else if(a==1)
            this.postac = "ace";
        else this.postac="karta zostala zle zinicjowana";
    }
    // wylicza i ustawia wartosc karty w BlackJack (dla asa przyjeto 11)
    void wyliczWartoscWBlackJack(int b){
        int a=(b%52)%13;
        if (a>=2 && a<=10)
            this.wartoscWBlackJack = this.wartosc;
        else if(a==11)
            this.wartoscWBlackJack = 10;
        else if(a==12)
            this.wartoscWBlackJack = 10;
        else if(a==0)
            this.wartoscWBlackJack = 10;
        else if(a==1)
            this.wartoscWBlackJack = 11;
    }
    //zwraca pelna nazwe karty (wg png)
    public String getFullName(){
        return postac+"_of_"+kolor;
    }
    //drukuje parametry karty
    public void print(){
        System.out.println(" nr karty : "+nrKarty+" nr w tali: "+nrKartyWTali+" wartosc: "+wartosc+" wartosc w BlackJack: "+wartoscWBlackJack+" "+getFullName());
    }
    public int getWartosc(){return wartosc;}
    public int getWartoscWBlackJack (){return wartoscWBlackJack;}

    public void setVisible(boolean v){this.visible=v;}
    public boolean getVisible(){return this.visible;}




}
