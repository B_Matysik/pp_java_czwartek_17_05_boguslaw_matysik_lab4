

import java.io.*;
import java.net.Socket;
import java.util.Scanner;

public class Klient {

    public static Socket s;
    public static void main(String[] args) throws IOException{

        s = new Socket("localhost", 5555);
        PrintWriter output = new PrintWriter(s.getOutputStream(),true);
        Scanner input = new Scanner(s.getInputStream());
        Scanner odczyt = new Scanner(System.in);

        int glos=2;

        while(glos!=-1){
            glos=odczyt.nextInt();
            output.println(glos);
            System.out.println("klient wyslalem: " + glos);

            glos=input.nextInt();
            System.out.println("klient odebralem sume: " + glos);

        }
        input.close();
        s.close();
    }
}